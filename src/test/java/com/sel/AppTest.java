package com.sel;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class AppTest
{
@Test
public void	smokeTest(){
System.setProperty("webdriver.gecko.driver",
				"C:\\Users\\abhilaa\\Downloads\\geckodriver-v0.19.1-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		String url = "https://www.mozilla.org/en-US/";
		driver.get(url);
		try {
			Assert.assertEquals(url, driver.getCurrentUrl());
			System.out.println("Navigated to correct webpage");
		} catch (Throwable pageNavigationError) {
			System.out.println("Didn't navigate to correct webpage");
		}
		System.out.println("Success");
}
public static void main(String[] args) throws InterruptedException {
		AppTest test = new AppTest();
		test.smokeTest();
	}
}